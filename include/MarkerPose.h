/*
 * MarkerPose.h
 *
 *  Created on: 18-May-2016
 *      Author: krishna
 */
#ifndef MARKERPOSE_H_
#define MARKERPOSE_H_

#include "ros/ros.h"
#include <iostream>
#include <Eigen/Dense>
#include <aruco_msgs/MarkerArray.h>

MatrixXf GetMarkerpose(const aruco_msgs::MarkerArray::ConstPtr& MarkerArray);

#endif

