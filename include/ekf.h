/*
 * ekf.h
 *
 *  Created on: 18-May-2016
 *      Author: krishna
 */
#ifndef EKF_H_
#define EKF_H_

#include <geometry_msgs/Pose.h>
#include <Eigen/Dense>

using namespace Eigen;

void ekf(geometry_msgs::Pose* Nayan_pose, Vector3f accel_ef, Vector3f vel_ef, Vector3f vel_ef_stereo, MatrixXf marker_pose_qf, float dt, int update_ekf_marker, int px4_quality, int *ekf_switch, visualization_msgs::MarkerArray* marker_array, geometry_msgs::Vector3* Nayan_vel_ekf_ef);

#endif

