/*
 * sonar.h
 *
 *  Created on: 24-May-2016
 *      Author: krishna
 */
#ifndef SONAR_DATA_H
#define SONAR_DATA_H

#include "ros/ros.h"
#include <iostream>
#include "px_comm/OpticalFlow.h"

float GetSonarData(const px_comm::OpticalFlow::ConstPtr& Px4flowData);

#endif



