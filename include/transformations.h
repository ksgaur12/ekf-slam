/*
 * transformations.h
 *
 *  Created on: 18-May-2016
 *      Author: krishna
 */
#ifndef TRANSFORMATIONS_H_
#define TRANSFORMATIONS_H_


#include "ros/ros.h"
#include <iostream>
#include "sensor_msgs/Imu.h"
#include "geometry_msgs/Pose.h"
#include "px_comm/OpticalFlow.h"
#include <Eigen/Dense>

using namespace Eigen;


Vector3f GetAccelbftoef(const sensor_msgs::Imu::ConstPtr& ImuData, geometry_msgs::Pose* Nayan_pose);
Vector3f GetPx4flowDatabftoef(const px_comm::OpticalFlow::ConstPtr& Px4flowData);
Matrix3f GetrotMatbftoef(geometry_msgs::Pose* Nayan_pose);
float Getsonarbftoef(float sonarVal);

#endif
