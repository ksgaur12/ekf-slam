/*
 * MarkerPose.cpp
 *
 *  Created on: 18-May-2016
 *      Author: krishna
 */
#include "ros/ros.h"
#include <iostream>
#include <Eigen/Dense>
#include <aruco_msgs/MarkerArray.h>
#include <aruco_msgs/Marker.h>

using namespace Eigen;
using namespace std;

Vector3f nayan_cam_diff;

MatrixXf GetMarkerpose(const aruco_msgs::MarkerArray::ConstPtr& MarkerArray){

	float size;
	int i;
	size = MarkerArray->markers.size(); //getting the no of markers currently observed

	MatrixXf Marker_pose(4,(int)size);
	aruco_msgs::Marker marker;
	nayan_cam_diff <<  0.07, -0.02, -0.177;

	for (i=0; i<size; i++){
		//creating a eigen matrix of currently seen markers containing ids and pose info
		marker = MarkerArray->markers[i];
		Marker_pose.block<4,1>(0,i) <<marker.id,
				                      -marker.pose.pose.position.y + nayan_cam_diff[0],
									  -marker.pose.pose.position.x + nayan_cam_diff[1],
									  -marker.pose.pose.position.z + nayan_cam_diff[2];

	}

	//cout << Marker_pose << endl<<endl;
	return Marker_pose;
}




