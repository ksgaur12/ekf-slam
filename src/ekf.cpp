#include "ros/ros.h"
#include <fstream>
#include <geometry_msgs/Pose.h>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include "transformations.h"
#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>

using namespace Eigen;
using namespace std;

Vector3f nayan_set_origin;
VectorXf x_hat(6), x(6); //state vector includes x, y, z pose and their derivatives in ef reference

MatrixXf sig_hat(6,6), sig(6,6); //covariance associated with the state
MatrixXf marker_association(2,1); //a marker array, keeps track of all the previously observed markers

int m=0; //a count variable
int marker_count=1; //a count for the no of makers in the visualization_msgs::MarkerArray
int switch_hlp=0;

void Initialize(){
	sig_hat.setZero();
	sig_hat = MatrixXf::Identity(6,6);
	sig_hat.block<3,3>(3,3) = 6 * MatrixXf::Identity(3,3);
	sig = MatrixXf::Identity(6,6);
	//set all the state and covariance elements to zero
	x.setZero();
	marker_association.setZero();
	m++;
}


void Prediction_step(Vector3f accel_ef, float dt){

	MatrixXf A(6,6);
	MatrixXf B(6,3);
	MatrixXf sig_(6,6);
	MatrixXf accel_err(3,3);

	//accel variance matrix
	accel_err << 0.0542934912, 0, 0,
				 0, 0.0684527805, 0,
				 0, 0, 0.0859790204;

	int size_x;
	size_x = x.size();//getting the current size of the state

	MatrixXf F(6,size_x);

	VectorXf u(6);

	F.setZero();
	F.block<6,6>(0,0) = MatrixXf::Identity(6,6);
	//F maps the 1X6 state vector(which contains the pose and velocity) to the whole state vector

	A = MatrixXf::Identity(6,6);
	A.block<3,3>(0,3) << dt, 0, 0,
						 0, dt, 0,
						 0, 0, dt;

	B.block<3,3>(0,0) << dt*dt/2, 0, 0,
						 0, dt*dt/2, 0,
						 0, 0, dt*dt/2;

	B.block<3,3>(3,0) << dt, 0, 0,
			 	 	 	 0, dt, 0,
						 0, 0, dt;



	u = x.head(6); //u contains the first 6 elements of the state vector(which contains robot info)

	sig_ = sig.block<6,6>(0,0);
	accel_ef[2] = accel_ef[2]; //eliminating the gravity from the accel z

	x_hat.head(6) << 0,0,0,0,0,0; //state vector now contains the marker info only
	x_hat = x_hat + F.transpose()*(A*u + B*accel_ef); //getting the predicted state (does not affect the marker info)

	sig_hat.block<6,6>(0,0) = MatrixXf::Zero(6,6);
	sig_hat = sig_hat + F.transpose()*(A*sig_*A.transpose() + B*accel_err*B.transpose())*F; //getting the predicted covariance

	x = x_hat;
	sig = sig_hat;


}

void marker_visualization_pub(int id, visualization_msgs::MarkerArray* marker_array){

	visualization_msgs::Marker marker;
	marker_array->markers.resize(marker_count);

	marker.header.frame_id = "map";
	marker.header.stamp = ros::Time::now();

	// Set the namespace and id for this marker.  This serves to create a unique ID
	// Any marker sent with the same namespace and id will overwrite the old one
    marker.ns = "aruco_marker";
	marker.id = id;

	// Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
	marker.type = visualization_msgs::Marker::SPHERE;

	 //Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
	 marker.action = visualization_msgs::Marker::ADD;

	 // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
	 Vector3f marker_pose;
	 marker_pose = x_hat.segment<3>(id);
	 marker.pose.position.x = marker_pose[0];
	 marker.pose.position.y = marker_pose[1];
	 marker.pose.position.z = marker_pose[2];
	 marker.pose.orientation.x = 0.0;
	 marker.pose.orientation.y = 0.0;
	 marker.pose.orientation.z = 0.0;
	 marker.pose.orientation.w = 1.0;


	 Matrix3Xf marker_covariance;
	 marker_covariance = sig_hat.block<3,3>(id,id);

	 SelfAdjointEigenSolver<MatrixXf> es(marker_covariance);

	 Vector3f eigenval;
	 eigenval = es.eigenvalues();

	 // Set the scale of the marker -- 1x1x1 here means 1m on a side
	 marker.scale.x = 1 *eigenval[1];
	 marker.scale.y = 1 *eigenval[2];
	 marker.scale.z = 1 *eigenval[0];

	 // Set the color -- be sure to set alpha to something non-zero!
	 marker.color.r = 0.0f;
	 marker.color.g = 1.0f;
	 marker.color.b = 0.0f;
	 marker.color.a = 1.0;

	 marker.lifetime = ros::Duration();
	
	 int i;
	 for(i=0;i<marker_count;i++){
		if (id == marker_array->markers[i].id){
			marker_array->markers[i] = marker;
			break;		
		}
	 }
	 if (i==marker_count){
	 	 marker_array->markers[marker_count-1] = marker;
	 	 marker_count++;
	 }

}

void Measurement_update_markers(geometry_msgs::Pose* Nayan_pose, MatrixXf marker_pose_qf, visualization_msgs::MarkerArray* marker_array){

	MatrixXf Q(3,3);
	//measurement error matrix
	Q << 0.0004118985, 0, 0,
		 0, 0.0004792499, 0,
		 0, 0, 0.0002148933;

	MatrixXf marker_ids(3,marker_pose_qf.cols());
	marker_ids.setZero();
	marker_ids.row(0) = marker_pose_qf.row(0);

	int N;
	int i,j;

	N = marker_association.cols();
	//checking if the marker is active, passive or new
	for(i=0;i<marker_ids.cols();i++){
		for(j=0;j<N;j++){
			if((marker_ids(0,i) == marker_association(0,j))&&(j!=0)){//leave the first column of marker association
				marker_ids(1,i) = marker_association(1,j);
				marker_ids(2,i) = 1; //mark the maker with 1 if it is already present in the state vector
			}
		}
	}

	Matrix3f rotMatbftoef;
	rotMatbftoef = GetrotMatbftoef(Nayan_pose); //get the rotation matrix for transformation from bf to ef

	for(i=0;i<marker_ids.cols();i++){
		if(marker_ids(2,i)==0){ //if the marker is never seen before
			//then add the marker in the state vector(state of the marker is 3X1 vector contains x,y,z ef info of the marker)
			x_hat.conservativeResize(x_hat.size()+3);
			x_hat.tail(3) << marker_pose_qf(1,i), marker_pose_qf(2,i), marker_pose_qf(3,i);

			x_hat.tail(3) =  rotMatbftoef * x_hat.tail(3);

			x_hat.tail(3) = x_hat.tail(3) + x_hat.head(3);

			sig_hat.conservativeResizeLike(MatrixXf::Zero(x_hat.size(),x_hat.size()));
			sig_hat.bottomRightCorner(3,3) =0.3* MatrixXf::Identity(3,3); //extend the covariance matrix

			marker_association.conservativeResize(marker_association.rows(),marker_association.cols()+1);//extend the marker association matrix
			marker_association(0,marker_association.cols()-1) = marker_ids(0,i);//put the marker id of the new marker in the marker association matrix
			marker_association(1,marker_association.cols()-1) = x_hat.size()-3;//put the row no associated with the marker in the state vector

		}


		else{ //if the marker had already been observed before
			Vector3f z_hat;

			z_hat = x_hat.segment<3>(marker_ids(1,i)) - x_hat.head(3); //predicted value of the marker as seen from the bf
			

			int size_x, N;

			size_x = x_hat.size();
			N = (size_x - 6)/3;//no of markers

			MatrixXf F(6,size_x);
			F.setZero();
			F.topLeftCorner(3,3).setIdentity();
			F.block<3,3>(3,marker_ids(1,i)) = MatrixXf::Identity(3,3);
			//F maps the current pose and marker pose info to the whole state vector

			MatrixXf h(3,6);
			h << -1, 0, 0, 1, 0, 0,
				 0, -1, 0, 0, 1, 0,
				 0, 0, -1, 0, 0, 1;


			MatrixXf H(3,size_x);
			H = h*F;

			MatrixXf S(3,3);
			S = H*sig_hat*H.transpose() + Q;

			MatrixXf K(size_x,3);
			K = sig_hat * H.transpose() * S.inverse(); //computing the kalman gain associated with the particular observed marker

			VectorXf z(3);
			z = rotMatbftoef * marker_pose_qf.block<3,1>(1,i); //getting the current sensor estimate of the marker

			x_hat = x_hat + K * (z - z_hat); //getting the corrected state vector

			sig_hat = (MatrixXf::Identity(size_x,size_x) - K*H)*sig_hat; //getting the new covariance matrix
			marker_visualization_pub(marker_ids(1,i), marker_array);
		}
	}

	int size_x;
	size_x = x_hat.size();
	x.resize(size_x);
	sig.resize(size_x, size_x);
	x = x_hat;//final estimate based on all the marker estimates
	sig = sig_hat;//final covariance matrix
}

void Measurement_update_px4flow(geometry_msgs::Pose* Nayan_pose, Vector3f vel_ef){

	int size_x;
	size_x = x_hat.size();

	//measurement noise for velocity in x and y
	MatrixXf Q(2,2);
	Q << 0.0232811033, 0,
		 0, 0.0823707668;

	MatrixXf h;
	h = MatrixXf::Identity(2,2);

	MatrixXf F(2,size_x);
	F.setZero();
	F.block<2,2>(0,3) = MatrixXf::Identity(2,2);
	//F maps the velocity in x and y to whole state vector

	VectorXf z_hat(2);
	z_hat = F*x_hat; //measurement predication

	MatrixXf H(2,size_x);
	H = h*F;

	MatrixXf  S(2,2);
	S = H*sig_hat*H.transpose() + Q;

	MatrixXf K(size_x,2);
	K = sig_hat*H.transpose()*S.inverse();//kalman gain

	Vector2f vel_ef_3d_2d(2);
	vel_ef_3d_2d << vel_ef[0], vel_ef[1];//getting just the veloctiy output

	x = x_hat + K*(vel_ef_3d_2d - z_hat); //updated state
	sig = (MatrixXf::Identity(size_x, size_x) - K*H)*sig_hat; //updated covariance

	x_hat = x;
	sig_hat = sig;

}

void Measurement_update_z(Vector3f vel_ef){

	float Q; //noise in sonar
	Q = 0.00084853043;

	int size_x;
	size_x = x_hat.size();

	VectorXf h(size_x);
	h.setZero();

	h[2] = 1;

	float z_hat;
	z_hat = h.transpose()*x_hat;//predicted height

	float z;
	z = -vel_ef[2];

	float S;
	S = h.transpose()*sig_hat*h + Q;

	VectorXf K(size_x);
	K = sig_hat*h/S; //kalman gain

	//updated state and covariance
	x = x_hat + K*(z - z_hat);
	sig = (MatrixXf::Identity(size_x,size_x) - K*h.transpose())*sig_hat;

	x_hat = x;
	sig_hat = sig;

}

void Measurement_update_stereo_odom(geometry_msgs::Pose* Nayan_pose, Vector3f vel_ef_stereo){

	int size_x;
	size_x = x_hat.size();

	//measurement noise for velocity in x and y
	MatrixXf Q(2,2);
	Q << 0.0232811033, 0, 0,
		 0, 0.0223707668, 0,
		 0, 0, 0.024547842;

	MatrixXf h;
	h = MatrixXf::Identity(3,3);

	MatrixXf F(3,size_x);
	F.setZero();
	F.block<3,3>(0,3) = MatrixXf::Identity(3,3);
	//F maps the velocity in x, y and z to whole state vector

	VectorXf z_hat(3);
	z_hat = F*x_hat; //measurement predication

	MatrixXf H(3,size_x);
	H = h*F;

	MatrixXf  S(3,3);
	S = H*sig_hat*H.transpose() + Q;

	MatrixXf K(size_x,3);
	K = sig_hat*H.transpose()*S.inverse();//kalman gain

	x = x_hat + K*(vel_ef_stereo - z_hat); //updated state
	sig = (MatrixXf::Identity(size_x, size_x) - K*H)*sig_hat; //updated covariance

	x_hat = x;
	sig_hat = sig;

}

void ekf(geometry_msgs::Pose* Nayan_pose, Vector3f accel_ef, Vector3f vel_ef, Vector3f vel_ef_stereo, MatrixXf marker_pose_qf, float dt, int update_ekf_marker, int px4_quality, int *ekf_switch, visualization_msgs::MarkerArray* marker_array, geometry_msgs::Vector3* Nayan_vel_ekf_ef){

	if (m ==0){
		Initialize(); //initialization of ekf
		nayan_set_origin <<0,0,0;
	}

	Prediction_step(accel_ef, dt); //kalman prediction step based on the Newtonian physics
	

	if(vel_ef[2]>-5.0&&vel_ef[2]<-0.30){ //check if the sonar estimate is in the possible range
			Measurement_update_z(vel_ef); //update the measurement based on sonar for z estimate
		}

	if(px4_quality>20){
		Measurement_update_px4flow(Nayan_pose, vel_ef);
	}
	
	if (update_ekf_marker == 1 && m > 2){
		Measurement_update_markers(Nayan_pose, marker_pose_qf,marker_array); //update the measurement if marker info is available
	}



	//Measurement_update_stereo_odom(Nayan_pose, vel_ef_stereo);

	if (*ekf_switch){

		nayan_set_origin << x.head(3);
		nayan_set_origin[2] = 0;
		switch_hlp = 1;

		x[3] = 0;
		x[4] = 0;
		x[5] = 0;
	}
	//final nayan pose in the ef
	if (switch_hlp){
		Nayan_pose->position.x = x[0] - nayan_set_origin[0];
		Nayan_pose->position.y = x[1] - nayan_set_origin[1];
		Nayan_pose->position.z = x[2] - nayan_set_origin[2];

		Nayan_vel_ekf_ef->x = x[3];
		Nayan_vel_ekf_ef->y = x[4];
		Nayan_vel_ekf_ef->z = x[5];
	}
	else{
		Nayan_pose->position.x = 0;
		Nayan_pose->position.y = 0;
		Nayan_pose->position.z = x[2];

		Nayan_vel_ekf_ef->x = 0;
		Nayan_vel_ekf_ef->y = 0;
		Nayan_vel_ekf_ef->z = 0;
	}
	m++;
	//cout << Nayan_pose->position <<endl<<endl;

}
