#include "ros/ros.h"
#include <iostream>
#include <math.h>
#include <Eigen/Dense>
#include <aruco/aruco.h>
#include <aruco/cvdrawingutils.h>

#include <aruco_msgs/MarkerArray.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Vector3.h>
#include <mavros_msgs/RCIn.h>
#include <nav_msgs/Path.h>
#include <px_comm/OpticalFlow.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Float64.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <tf/tf.h>
#include <tf/transform_datatypes.h>
#include <visualization_msgs/MarkerArray.h>

#include "transformations.h"
#include "MarkerPose.h"
#include "ekf.h"
#include "sonar_data.h"
#include <aruco_ros/aruco_ros_utils.h>

using namespace Eigen;
using namespace std;
using namespace aruco;
using namespace aruco_ros;

aruco_msgs::MarkerArray::Ptr marker_msg_;
geometry_msgs::Pose Nayan_pose;
geometry_msgs::PoseStamped Nayan_slave_pose;
geometry_msgs::PoseStamped Nayan_stamped_pose;
geometry_msgs::Vector3 Nayan_accel_ef;
geometry_msgs::Vector3 Nayan_accel_bf;
geometry_msgs::Vector3 Nayan_vel_ef;
geometry_msgs::Vector3 Nayan_vel_ekf_ef;
nav_msgs::Path Nayan_rviz_path;
visualization_msgs::MarkerArray marker_array; //array of marker for rviz visualization

Vector3f accel_ef, sum_accel_ef;
Vector3f vel_ef, sum_vel_ef, vel_ef_stereo;
MatrixXf marker_pose_qf(4,1);

int current_marker_seq=0;
int previous_marker_seq=0;
int update_ekf_marker = 0;
int px4_quality=0;
int ekf_switch = 0;
int rviz_count = 1;
int prev_rc6_val= 0;

float loop_freq = 40;
float dt;

double R, P,Y;
double marker_size=0.150;
double current_time, previous_time = 0.0;

bool cam_info_received = false;
bool useRectifiedImages = true;

vector<Marker> markers_;
MarkerDetector mDetector;
CameraParameters camParam;

ros::Publisher image_pub;
ros::Publisher marker_pub_;
ros::Publisher position_pub;

tf::StampedTransform rightToLeft;
ros::Subscriber cam_info_sub;


static const std::string OPENCV_WINDOW = "Image window";

void GetImudata(const sensor_msgs::Imu::ConstPtr& ImuData){

	Nayan_pose.orientation = ImuData->orientation;

	Nayan_stamped_pose.header = ImuData->header;

	Nayan_accel_bf.x = ImuData->linear_acceleration.x;
	Nayan_accel_bf.y = ImuData->linear_acceleration.y;
	Nayan_accel_bf.z = ImuData->linear_acceleration.z;

	Nayan_accel_ef.x = accel_ef[0];
	Nayan_accel_ef.y = accel_ef[1];
	Nayan_accel_ef.z = accel_ef[2];

	double q0, q1, q2, q3;
	q0 = Nayan_pose.orientation.w;
	q1 = Nayan_pose.orientation.x;
	q2 = Nayan_pose.orientation.y;
	q3 = Nayan_pose.orientation.z;


	/*R= atan2(2*(q0*q1+q2*q3), 1 -2*q1*q1-2*q2*q2);
	P= asin(2*(q0*q2-q1*q3));
	Y= atan2(2*(q0*q3+q1*q2), 1-2*q2*q2-2*q3*q3);
	Y= Y-1.57; */
	
	tf::Quaternion q;
	tf::Matrix3x3 Rotmat;
	tf::quaternionMsgToTF(Nayan_pose.orientation, q);
	Rotmat.setRotation(q);
	Rotmat.getRPY(R,P,Y);

	Y = Y-1.57;
	tf::Quaternion quat;
	quat.setRPY(R,P,Y);
	quat = quat.normalize();
	tf::quaternionTFToMsg(quat, Nayan_pose.orientation); //rotating the frame by pi/2

	//cout<<"R\t"<<R*180/3.14<<endl;
	//cout<<"P\t"<<P*180/3.14<<endl;
	//cout<<"Y\t"<<Y*180/3.14<<endl<<endl;
	//cout << accel_ef << endl<< endl;

	accel_ef = GetAccelbftoef(ImuData, &Nayan_pose);  //transformation of accel data from bf to ef

}

void Getpx4flowdata(const px_comm::OpticalFlow::ConstPtr& Px4flowData){

	vel_ef = GetPx4flowDatabftoef(Px4flowData); //transformation of velocity data from bf to ef
	vel_ef[2] = GetSonarData(Px4flowData);  //median filter for sonar data and transformation form bf to ef
	//vel_ef is sonar_z in eff

	px4_quality = Px4flowData->quality;
	Nayan_vel_ef.x = vel_ef[0];
	Nayan_vel_ef.y = vel_ef[1];
	Nayan_vel_ef.z = vel_ef[2];

}

void GetMarkerdata(const aruco_msgs::MarkerArray::Ptr& MarkerArray){

	float size;
	current_marker_seq = MarkerArray->header.seq;
	size = MarkerArray->markers.size();

	if(current_marker_seq!=previous_marker_seq){
		marker_pose_qf.resize(4,size);
		marker_pose_qf = GetMarkerpose(MarkerArray); //getting the array of all the marker with row(0)=id and pose vector
		update_ekf_marker = 1; //used to update ekf only if the previous data is not same as the new

	}


	else{
		update_ekf_marker = 0;
	}

	previous_marker_seq = current_marker_seq;
}

void imageCallback(const sensor_msgs::Image::ConstPtr& image){

	if(cam_info_received){
		ros::Time curr_stamp(ros::Time::now());
		cv_bridge::CvImagePtr cv_ptr;
		try
		{
			cv_ptr = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::RGB8); //pointer to the image in opencv

			cv::Mat inImage;
			cv::Mat im_grey;
			cv::cvtColor(cv_ptr->image,im_grey,CV_RGB2GRAY); //converting the image to grayscale

			markers_.clear();
			mDetector.detect(im_grey, markers_, camParam, marker_size, false); //detecting the markers


			if(cam_info_received){
			//marker data into marker msg
	        marker_msg_->markers.clear();
	        marker_msg_->markers.resize(markers_.size());
	        marker_msg_->header.stamp = curr_stamp;
	        marker_msg_->header.seq++;


	        for(size_t i=0; i<markers_.size(); ++i){
	            aruco_msgs::Marker & marker_i = marker_msg_->markers.at(i);
	            marker_i.header.stamp = curr_stamp;
	            marker_i.id = markers_.at(i).id;
	            marker_i.confidence = 1.0;
	          }



	        	  tf::StampedTransform cameraToReference;
	        	  cameraToReference.setIdentity();

	        	  for(size_t i=0; i<markers_.size(); ++i){
	        		  //geting the pose of each marker relative to the reference frame(camera in our case)
	        		  aruco_msgs::Marker & marker_i = marker_msg_->markers.at(i);
	        		  tf::Transform transform = arucoMarker2Tf(markers_[i]);
	        		  transform = static_cast<tf::Transform>(cameraToReference) * transform;
	                  tf::poseTFToMsg(transform, marker_i.pose.pose);
	                  marker_i.header.frame_id = "reference_frame";
	             }


	          if (marker_msg_->markers.size() > 0){
	        	  GetMarkerdata(marker_msg_); //geting the marker data in form of a eigen matrix
	        	  marker_pub_.publish(marker_msg_); //publishing the marker data
	          }
			}
			for(size_t i=0; i<markers_.size(); ++i){
		          markers_[i].draw(im_grey,cv::Scalar(0,0,255),2); //drawing the rectangle around each marker
			}

			if(camParam.isValid() && marker_size!=-1){
				for(size_t i=0; i<markers_.size(); ++i){
			        CvDrawingUtils::draw3dCube(im_grey, markers_[i], camParam);//drawing a 3d axis on each marker
			    }
			}


			if(image_pub.getNumSubscribers() > 0){
				//publishing the image
				cv_bridge::CvImage out_msg;
			    out_msg.header.stamp = curr_stamp;
			    out_msg.encoding = sensor_msgs::image_encodings::RGB8;
			    cv::cvtColor(im_grey, inImage, cv::COLOR_GRAY2BGR); //converting the image again into RGB from grayscale for publishing
			    out_msg.image = inImage;
			    image_pub.publish(out_msg.toImageMsg());

			}
		}

		catch(cv_bridge::Exception& e){
			ROS_ERROR("cv_bridge exception: %s", e.what());
			return;
			}
	}
}

void camCallback(const sensor_msgs::CameraInfo& cam_info){

	camParam = rosCameraInfo2ArucoCamParams(cam_info, useRectifiedImages);
	rightToLeft.setIdentity();
	rightToLeft.setOrigin(tf::Vector3(-cam_info.P[3]/cam_info.P[0],-cam_info.P[7]/cam_info.P[5],0.0)); //setting the origin if camera parameters are available (if camera calibartion done)

    cam_info_received = true;
	cam_info_sub.shutdown(); //if camera info is subscribed once then shutdown this subscriber.
}

void stereoOdomCallback(const nav_msgs::Odometry::ConstPtr& odom){

	vel_ef_stereo[0] = odom->twist.twist.linear.x;
	vel_ef_stereo[1] = odom->twist.twist.linear.y;
	vel_ef_stereo[2] = odom->twist.twist.linear.z;

}


void cam_info_callback(const sensor_msgs::CameraInfo::ConstPtr& cam_info){

	geometry_msgs::Quaternion quat;

	/*tf::Transform transform;
	transform.setOrigin( tf::Vector3(0, 0, 0));
	tf::Quaternion q;
	q.setRPY(0,0,0);
	transform.setRotation(q);
	br.sendTransform(tf::StampedTransform(transform, cam_info->header.stamp, "base_footprint", "base_link"));

	static tf::TransformBroadcaster br2;   //getting the transformation between base_link and stereo camera base
	tf::Transform transform2;
	transform2.setOrigin( tf::Vector3(0,0,0.1));
	tf::Quaternion q2;
	q2.setRPY(0,0,0);
	transform2.setRotation(q2);
	br2.sendTransform(tf::StampedTransform(transform2, cam_info->header.stamp, "base_link", "stereo_camera_base"));

	static tf::TransformBroadcaster br3;  //getting the trasformation between stereo camera base and left camera
	tf::Transform transform3;
	transform3.setOrigin( tf::Vector3(0, 0, 0));
	tf::Quaternion q3;
	q3.setRPY(-3.14/2,0,-3.14/2);
	transform3.setRotation(q3);
	br3.sendTransform(tf::StampedTransform(transform3, cam_info->header.stamp, "stereo_camera_base", "/stereo/left"));*/

	static tf::TransformBroadcaster br;   //getting the transformation between base_footprint and base_link
	tf::Transform transform;
	transform.setOrigin( tf::Vector3(0, 0, Nayan_pose.position.z));
	tf::Quaternion q;
	q.setRPY(R,P,0);
	transform.setRotation(q);
	br.sendTransform(tf::StampedTransform(transform, cam_info->header.stamp, "base_footprint", "base_link"));

	static tf::TransformBroadcaster br2;   //getting the transformation between base_link and stereo camera base
	tf::Transform transform2;
	transform2.setOrigin( tf::Vector3(0,0,0.1));
	tf::Quaternion q2;
	q2.setRPY(0,0,0);
	transform2.setRotation(q2);
	br2.sendTransform(tf::StampedTransform(transform2, cam_info->header.stamp, "base_link", "stereo_camera_base"));

	static tf::TransformBroadcaster br3;  //getting the trasformation between stereo camera base and left camera
	tf::Transform transform3;
	transform3.setOrigin( tf::Vector3(0, 0, 0));
	tf::Quaternion q3;
	q3.setRPY(-3.14/2,0,-3.14/2);
	transform3.setRotation(q3);
	br3.sendTransform(tf::StampedTransform(transform3, cam_info->header.stamp, "stereo_camera_base", "/stereo/left"));

	static tf::TransformBroadcaster br4;   //getting the transformation between base_footprint and base_link
	tf::Transform transform4;
	transform4.setOrigin( tf::Vector3(Nayan_pose.position.x, Nayan_pose.position.y, 0));
	tf::Quaternion q4;
	q4.setRPY(0,0,Y);
	transform4.setRotation(q4);
	br4.sendTransform(tf::StampedTransform(transform4, cam_info->header.stamp, "map", "base_footprint"));

}

void rcCallback(const mavros_msgs::RCIn::ConstPtr& rc_in){

	int rc6_val;
	rc6_val = rc_in->channels[5];

	if (rc6_val>1500 && prev_rc6_val<1500){
		ekf_switch=1;
		prev_rc6_val = rc6_val;
	}
	else if(rc6_val<1500 && prev_rc6_val>1500){
		ekf_switch=0;
		prev_rc6_val = rc6_val;
	}
	else {
		ekf_switch = 0;
	}
}

void rviz_stuff(){

	Nayan_rviz_path.poses.resize(rviz_count);
	Nayan_rviz_path.header.seq = rviz_count;
	Nayan_rviz_path.header.frame_id = "map";

	Nayan_rviz_path.poses[rviz_count-1].pose = Nayan_pose;

	rviz_count++;

}

void publishSensorPos(ros::Time stamp, bool flag_cv_active)
{
  tf::Matrix3x3 R;

  if(flag_cv_active)
    R.setRPY(1.0f, 0.0f, -Y);              //Hard coded similar thing on HLP implyinng CV is active
  else
    R.setRPY(0.0f, 0.0f, -Y);              //Hard coded similar thing on HLP implying CV in inactive

  tf::Quaternion q;
  R.getRotation(q);
  // copy the position and orientation to the pose message
  tf::quaternionTFToMsg(q, Nayan_slave_pose.pose.orientation);
  Nayan_slave_pose.header.stamp = stamp;
  Nayan_slave_pose.pose.position.x =  Nayan_pose.position.x;
  Nayan_slave_pose.pose.position.y = -Nayan_pose.position.y;
  Nayan_slave_pose.pose.position.z = -Nayan_pose.position.z;

  position_pub.publish(Nayan_slave_pose);
}


int main(int argc, char **argv){

  ros::init(argc, argv, "main");
  ros::NodeHandle n;
  ros::NodeHandle it;

  ros::Subscriber Imu_data_sub = n.subscribe("/mavros/imu/data", 1000, GetImudata); //subscribing imu data(accelerometer data + orientation)
  ros::Subscriber Px4flow_data_sub = n.subscribe("/px4flow/opt_flow",1000,Getpx4flowdata); //velocity x and y data along with ground distance data
  ros::Subscriber Image_sub = n.subscribe("/usb_cam/image_raw",1,imageCallback); //getting the image and detecting the markers
  ros::Subscriber Sterer_odom_sub = n.subscribe("/stereo_odometery",100,stereoOdomCallback);
  ros::Subscriber cam_info = n.subscribe("/usb_cam/camera_info", 1, &cam_info_callback);
  ros::Subscriber rc_in = n.subscribe("/mavros/rc/in",100,rcCallback);
  cam_info_sub = n.subscribe("/usb_cam/camera_info",1,camCallback); //getting the camera info for camera parameters (calibration of camera is required)


  ros::Publisher accel_ef_pub = n.advertise<geometry_msgs::Vector3>("nayan/accel_ef",100); //publishing the accelerometer data in ef
  ros::Publisher accel_bf_pub = n.advertise<geometry_msgs::Vector3>("nayan/accel_bf",100); //publishing the accelerometer data in ef
  ros::Publisher nayan_pose_pub = n.advertise<geometry_msgs::Pose>("nayan/pose",100); //publishing the nayan ekf pose along with the current orientation relative to the ef
  ros::Publisher vel_ef_pub = n.advertise<geometry_msgs::Vector3>("nayan/vel_ef",100); //publishing the velcity data in  the earth frame
  ros::Publisher rviz_path_pub = n.advertise<nav_msgs::Path>("nayan/rviz/path",100);//publishing the path of the nayan for visulaization on rviz
  ros::Publisher nayan_stamped_pose_pub = n.advertise<geometry_msgs::PoseStamped>("/nayan/pose/stamped", 200);//publishing the pose of the nayan for to be passed to HLP through mavros
  ros::Publisher nayan_vel_ekf_ef_pub = n.advertise<geometry_msgs::Vector3>("/nayan/vel/ekf",200);//publishing the velocity estimates from the ekf
  image_pub = it.advertise<sensor_msgs::Image>("aruco/image",1); //publishing the image after marker detection (drawn rectangle along with axis)
  marker_pub_ = it.advertise<aruco_msgs::MarkerArray>("aruco/markers", 100); //publishing the marker pose along with the orientation in the reference frame
  ros::Publisher marker_rviz_pub = n.advertise<visualization_msgs::MarkerArray>("visualization_marker", 100); //ros publisher for marker visualization
  position_pub = n.advertise<geometry_msgs::PoseStamped>("/mavros/vision_pose_ned/pose", 200);//publishing the pose of the nayan for to be passed to HLP through mavros

  ros::Rate loop_rate(loop_freq);

  int count = 0;

  while (ros::ok())
  {
	  current_time = ros::Time::now().toSec();

	  if (count==0){
		  marker_msg_ = aruco_msgs::MarkerArray::Ptr(new aruco_msgs::MarkerArray()); //creating a new markerarray msg
		  marker_msg_->header.frame_id = "reference_frame";
		  marker_msg_->header.seq =0;
		  sum_accel_ef.setZero();
		  sum_vel_ef.setZero();
   		  
		  Nayan_slave_pose.pose.orientation.x = 0;
		  Nayan_slave_pose.pose.orientation.y = 0;
		  Nayan_slave_pose.pose.orientation.z = 0;
		  Nayan_slave_pose.pose.orientation.w = 1;	   
	}

	 dt = (float)(current_time-previous_time);
	 if(count>450){
     	 // accel_ef = accel_ef-sum_accel_ef/450; //subtrating the initial accel offsets

		 //vel_ef =  vel_ef-sum_vel_ef/100; //subtracting the intial velocity offsets

		 ekf(&Nayan_pose, accel_ef - sum_accel_ef/450, vel_ef, vel_ef_stereo, marker_pose_qf, dt, update_ekf_marker, px4_quality, &ekf_switch, &marker_array, &Nayan_vel_ekf_ef); // Extended kamlan filter for localization of the nayan quadrotor


		 publishSensorPos(ros::Time::now(), true);

		 Nayan_stamped_pose.pose.position = Nayan_pose.position;

		 rviz_stuff();
	 }
	 else{
		 sum_accel_ef += accel_ef;
		 sum_vel_ef += vel_ef;
	 }

	 update_ekf_marker = 0;
	accel_ef_pub.publish(Nayan_accel_ef);
	accel_bf_pub.publish(Nayan_accel_bf);
	vel_ef_pub.publish(Nayan_vel_ef);
	nayan_pose_pub.publish(Nayan_pose);
	marker_rviz_pub.publish(marker_array);
	rviz_path_pub.publish(Nayan_rviz_path);
    nayan_stamped_pose_pub.publish(Nayan_stamped_pose);
    nayan_vel_ekf_ef_pub.publish(Nayan_vel_ekf_ef);

    ros::spinOnce();
    loop_rate.sleep();

    previous_time = current_time;
    ++count;
  }


  return 0;
}
