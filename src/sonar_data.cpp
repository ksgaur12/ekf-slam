/*
 * sonar.cpp
 *
 *  Created on: 24-May-2016
 *      Author: krishna
 */

#include "ros/ros.h"
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <px_comm/OpticalFlow.h>
#include "transformations.h"

using namespace Eigen;
using namespace std;

float sonarVal=0;
float threshold=0.3;
float set_count=0;
VectorXf sonar_set(40);
float nayan_sonar_z_diff = 0.105;
float start_sonar_val=0;
int first_val_check=0;
int no_sonar_data = 6;

float GetSonarData(const px_comm::OpticalFlow::ConstPtr& Px4flowData){

	int i;
	VectorXf sonar_set_copy(40);

	if (set_count==0){
		sonar_set.setZero();

	}
	sonar_set_copy = sonar_set;
	if (set_count < 40){
		//getting first 40 set of sonar data
		sonar_set_copy[set_count] = Px4flowData->ground_distance;
		sonar_set[set_count] = Px4flowData->ground_distance;
		set_count++;
		sonarVal = Px4flowData->ground_distance;
		if(set_count==39){
			first_val_check = 1;
		}
	}

	else{

		sort(sonar_set_copy.data(), sonar_set_copy.data()+sonar_set_copy.size()); //arranging the 400 set of data in increasing order
		if((Px4flowData->ground_distance > (sonar_set_copy[20]-threshold))&&(Px4flowData->ground_distance < (sonar_set_copy[20]+threshold))){ //checking if the current value is in a limit of the median value
			//if so then return the current sonar data
			sonarVal = Px4flowData->ground_distance;
			//adding the offset between camera and sonar
			sonarVal = Getsonarbftoef(sonarVal+nayan_sonar_z_diff);  //transform the data form bf to ef
			sonarVal = sonarVal;
			return sonarVal;
		}

		for(i=0;i<39;i++){

			sonar_set[i] = sonar_set[i+1]; // updating the sonar set for next 400 data
		}
		sonar_set[39] = Px4flowData->ground_distance;

	}

	return no_sonar_data;//return no sonar data if sonar data is an outlier
}

