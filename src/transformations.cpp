/*
 * transformation.cpp
 *
 *  Created on: 18-May-2016
 *      Author: krishna
 */
#include "ros/ros.h"
#include <iostream>
#include "sensor_msgs/Imu.h"
#include "geometry_msgs/Pose.h"
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <px_comm/OpticalFlow.h>

using namespace Eigen;
using namespace std;
Matrix3f rotation_bf_ef;


Vector3f GetAccelbftoef(const sensor_msgs::Imu::ConstPtr& ImuData, geometry_msgs::Pose* Nayan_pose){

	Vector3f accel_bf, accel_ef;
	Quaternionf q;

	//create a vector of accel data
	accel_bf << ImuData->linear_acceleration.x , ImuData->linear_acceleration.y, ImuData->linear_acceleration.z;

	//get the quaternion from the ros msg
	q = Quaternion<float>(Nayan_pose->orientation.w, Nayan_pose->orientation.x, Nayan_pose->orientation.y, Nayan_pose->orientation.z);
	q = q.normalized();
	rotation_bf_ef = q.toRotationMatrix(); //creating a rotation matrix from the quaternion

	Vector3f angles;
	angles = rotation_bf_ef.eulerAngles(0,1,2); //get the euler angles

	accel_ef = rotation_bf_ef * accel_bf; //transforming accel bf to ef

	return accel_ef;
}

Vector3f GetPx4flowDatabftoef(const px_comm::OpticalFlow::ConstPtr& Px4flowData){

	Vector3f vel_ef, vel_bf;

	vel_bf << Px4flowData->velocity_y, Px4flowData->velocity_x, 0; //create a vector of velocity bf data

	vel_ef = rotation_bf_ef * vel_bf; //transforming the bf velocity vector to the ef vector
	return vel_ef;
}

Matrix3f GetrotMatbftoef(geometry_msgs::Pose* Nayan_pose){
	Matrix3f rotation_bf_ef;
	Quaternionf q;

	//return the rotation matrix based on the orientation from the imu msg

	q = Quaternion<float>(Nayan_pose->orientation.w, Nayan_pose->orientation.x, Nayan_pose->orientation.y, Nayan_pose->orientation.z);
	q = q.normalized();

	rotation_bf_ef = q.toRotationMatrix();
	return rotation_bf_ef;

}

float Getsonarbftoef(float sonarVal){

	Vector3f pose_z_bf, pose_z_ef;

	pose_z_bf << 0,0,-sonarVal;
	pose_z_ef = rotation_bf_ef * pose_z_bf; //transforming sonar data ef to bf
	return pose_z_bf[2];
}
